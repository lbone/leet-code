//罗马数字包含以下七种字符： I， V， X， L，C，D 和 M。 
//
// 
//字符          数值
//I             1
//V             5
//X             10
//L             50
//C             100
//D             500
//M             1000 
//
// 例如， 罗马数字 2 写做 II ，即为两个并列的 1。12 写做 XII ，即为 X + II 。 27 写做 XXVII, 即为 XX + V + I
//I 。 
//
// 通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做 IIII，而是 IV。数字 1 在数字 5 的左边，所表示的数等于大数 5
// 减小数 1 得到的数值 4 。同样地，数字 9 表示为 IX。这个特殊的规则只适用于以下六种情况： 
//
// 
// I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。 
// X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。 
// C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。 
// 
//
// 给你一个整数，将其转为罗马数字。 
//
// 
//
// 示例 1: 
//
// 
//输入: num = 3
//输出: "III" 
//
// 示例 2: 
//
// 
//输入: num = 4
//输出: "IV" 
//
// 示例 3: 
//
// 
//输入: num = 9
//输出: "IX" 
//
// 示例 4: 
//
// 
//输入: num = 58
//输出: "LVIII"
//解释: L = 50, V = 5, III = 3.
// 
//
// 示例 5: 
//
// 
//输入: num = 1994
//输出: "MCMXCIV"
//解释: M = 1000, CM = 900, XC = 90, IV = 4. 
//
// 
//
// 提示： 
//
// 
// 1 <= num <= 3999 
// 
// Related Topics 哈希表 数学 字符串 
// 👍 665 👎 0
package com.wj.leetcode.leetcode.editor.cn;

public class IntegerToRoman12 {
    public static void main(String[] args) {
        Solution solution = new IntegerToRoman12().new Solution();
        System.out.println(solution.intToRoman(3));
        System.out.println(solution.intToRoman(4));
        System.out.println(solution.intToRoman(9));
        System.out.println(solution.intToRoman(58));
        System.out.println(solution.intToRoman(1994));
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public String intToRoman(int num) {
            String result = "";
            if (num > 0 && num < 4000) {
                int i = 1;//位数
                do {
                    int number = num % 10;
                    result = judge(number, i++) + result;
                } while ((num = num / 10) > 0);
            }
            return result;
        }

        public String judge(int num, int index) {
            StringBuilder sb = new StringBuilder();
            if (num == 4) {
                switch (index) {
                    case 1:
                        sb.append("IV");
                        break;
                    case 2:
                        sb.append("XL");
                        break;
                    case 3:
                        sb.append("CD");
                        break;
                    default:
                        break;
                }
            } else if (num == 9) {
                switch (index) {
                    case 1:
                        sb.append("IX");
                        break;
                    case 2:
                        sb.append("XC");
                        break;
                    case 3:
                        sb.append("CM");
                        break;
                    default:
                        break;
                }
            } else if (num >= 5) {
                switch (index) {
                    case 1:
                        sb.append("V");
                        break;
                    case 2:
                        sb.append("L");
                        break;
                    case 3:
                        sb.append("D");
                        break;
                    default:
                        break;
                }
                num = num - 5;
            }
            if (num > 0 && num < 4) {
                for (int i = 0; i < num; i++) {
                    String flag = "";
                    switch (index) {
                        case 1:
                            flag = "I";
                            break;
                        case 2:
                            flag = "X";
                            break;
                        case 3:
                            flag = "C";
                            break;
                        case 4:
                            flag = "M";
                            break;
                        default:
                            break;
                    }
                    sb.append(flag);
                }
            }
            return sb.toString();
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}