//给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。 
//
// 回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。例如，121 是回文，而 123 不是。 
//
// 
//
// 示例 1： 
//
// 
//输入：x = 121
//输出：true
// 
//
// 示例 2： 
//
// 
//输入：x = -121
//输出：false
//解释：从左向右读, 为 -121 。 从右向左读, 为 121- 。因此它不是一个回文数。
// 
//
// 示例 3： 
//
// 
//输入：x = 10
//输出：false
//解释：从右向左读, 为 01 。因此它不是一个回文数。
// 
//
// 示例 4： 
//
// 
//输入：x = -101
//输出：false
// 
//
// 
//
// 提示： 
//
// 
// -231 <= x <= 231 - 1 
// 
//
// 
//
// 进阶：你能不将整数转为字符串来解决这个问题吗？ 
// Related Topics 数学 
// 👍 1571 👎 0

package com.wj.leetcode.leetcode.editor.cn;
public class PalindromeNumber9 {
    public static void main(String[] args) {
        Solution solution = new PalindromeNumber9().new Solution();
        int x = 121;
        int x1 = -121;
        int x2 = 10;
        System.out.println(solution.isPalindrome(x));
        System.out.println(solution.isPalindrome(x1));
        System.out.println(solution.isPalindrome(11));
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public boolean isPalindrome(int x) {
            //负数都不是回文数
            //末尾是0的除了0本身其他都不是回文数
            if (x < 0 || (x % 10 == 0 && x != 0)) {
                return false;
            }
            //计算x是几位数
            int length = 1;
            int y = x;
            while ((y = y / 10) > 0) {
                length++;
            }
            //循环判断第一位和最后一位是否一致，不一致则返回false;
            //一致则去掉前后两位继续判断
            while (length > 1) {
                int pow = (int) Math.pow(10, length - 1);
                if (x % 10 == x / pow) {
                    length -= 2;
                    x = (x % pow) / 10;
                } else {
                    return false;
                }
            }
            return true;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}