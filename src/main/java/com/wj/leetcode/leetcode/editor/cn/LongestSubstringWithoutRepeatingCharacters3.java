//给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。 
//
// 
//
// 示例 1: 
//
// 
//输入: s = "abcabcbb"
//输出: 3 
//解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
// 
//
// 示例 2: 
//
// 
//输入: s = "bbbbb"
//输出: 1
//解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
// 
//
// 示例 3: 
//
// 
//输入: s = "pwwkew"
//输出: 3
//解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
//     请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
// 
//
// 示例 4: 
//
// 
//输入: s = ""
//输出: 0
// 
//
// 
//
// 提示： 
//
// 
// 0 <= s.length <= 5 * 104 
// s 由英文字母、数字、符号和空格组成 
// 
// Related Topics 哈希表 字符串 滑动窗口 
// 👍 5855 👎 0
package com.wj.leetcode.leetcode.editor.cn;

import java.util.HashSet;
import java.util.Set;

public class LongestSubstringWithoutRepeatingCharacters3 {
    public static void main(String[] args) {
        Solution solution = new LongestSubstringWithoutRepeatingCharacters3().new Solution();
        String s1 = "abcabcbb";
        String s2 = "bbbbbcdef";
        String s3 = "pwwkew";
        System.out.println(solution.lengthOfLongestSubstring(s1));
        System.out.println(solution.lengthOfLongestSubstring(s2));
        System.out.println(solution.lengthOfLongestSubstring(s3));
        System.out.println(solution.lengthOfLongestSubstring(null));
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int lengthOfLongestSubstring(String s) {
            char[] chars = s != null ? s.toCharArray() : new char[]{};
            Set<Character> set = new HashSet();//存储长度
            int length = set.size();
            for (int i = 0; i < chars.length && chars.length - i > length; i++) {//要保证未计算的长度大于已记录的最大长度
                for (int j = i; j < chars.length; j++) {
                    if (set.contains(chars[j])) {
                        break;
                    } else {
                        set.add(chars[j]);
                    }
                }
                length = Math.max(length, set.size());
                set.clear();
            }
            return length;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}