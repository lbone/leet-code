//将一个给定字符串 s 根据给定的行数 numRows ，以从上往下、从左到右进行 Z 字形排列。 
//
// 比如输入字符串为 "PAYPALISHIRING" 行数为 3 时，排列如下： 
//
// 
//P   A   H   N
//A P L S I I G
//Y   I   R 
//
// 之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如："PAHNAPLSIIGYIR"。 
//
// 请你实现这个将字符串进行指定行数变换的函数： 
//
// 
//string convert(string s, int numRows); 
//
// 
//
// 示例 1： 
//
// 
//输入：s = "PAYPALISHIRING", numRows = 3
//输出："PAHNAPLSIIGYIR"
// 
//示例 2：
//
// 
//输入：s = "PAYPALISHIRING", numRows = 4
//输出："PINALSIGYAHRPI"
//解释：
//P     I    N
//A   L S  I G
//Y A   H R
//P     I
// 
//
// 示例 3： 
//
// 
//输入：s = "A", numRows = 1
//输出："A"
// 
//
// 
//
// 提示： 
//
// 
// 1 <= s.length <= 1000 
// s 由英文字母（小写和大写）、',' 和 '.' 组成 
// 1 <= numRows <= 1000 
// 
// Related Topics 字符串 
// 👍 1227 👎 0

package com.wj.leetcode.leetcode.editor.cn;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ZigzagConversion6 {
    public static void main(String[] args) {
        Solution solution = new ZigzagConversion6().new Solution();
        String s = "PAYPALISHIRING";
        int numRows = 3;
        System.out.println(solution.convert(s, numRows));
        s = "PAYPALISHIRING";
        numRows = 4;
        System.out.println(solution.convert(s, numRows));

    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public String convert(String s, int numRows) {
            if (null != s && numRows > 1) {
                Map<Integer, StringBuilder> map = new HashMap<>();
                boolean flag = true;//标记是竖还是斜
                for (int i = 0, row = 0; i < s.length(); i++) {
                    if (map.get(row) == null) {
                        map.put(row, new StringBuilder());
                    }
                    map.get(row).append(s.charAt(i));
                    if (row == numRows - 1) {//切换斜
                        flag = false;
                    } else if (row == 0) {//切换竖
                        flag = true;
                    }
                    if (flag) {
                        row++;
                    } else {
                        row--;
                    }
                }
                return map.values().stream().map(StringBuilder::toString).collect(Collectors.joining());
            } else {
                return s;
            }
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}